var Sidebar = function ( editor ) {

	var meshCount = 0;

	var container = new UI.Panel();
	var child_container = new UI.Panel();

	child_container.add( new UI.Text( 'Controls' ) );
	container.add(child_container);

	var another_child 	=	new UI.Panel();
	
	var circle 			=	new UI.Circle('50px');
	circle.setId('add-circle');
	
	var rectangle		=	new UI.Rectangle('50px','25px');
	rectangle.setId('add-rect');
	
	var square		=	new UI.Rectangle('50px','50px');
	square.setId('add-square');

	var cube		=	new UI.Cube('75px','75px');
	cube.setId('add-cube');
		rectangle.onClick( function () {

		var width = 200;
		var height = 100;

		var widthSegments = 1;
		var heightSegments = 1;

		var geometry = new THREE.PlaneGeometry( width, height, widthSegments, heightSegments );
		var material = new THREE.MeshPhongMaterial();
		var mesh = new THREE.Mesh( geometry, material );
		mesh.name = 'Plane ' + ( ++ meshCount );

		mesh.rotation.x = - Math.PI/2;

		editor.addObject( mesh );
		editor.select( mesh );

	} );

	square.onClick( function () {

		var width = 200;
		var height = 200;

		var widthSegments = 1;
		var heightSegments = 1;

		var geometry = new THREE.PlaneGeometry( width, height, widthSegments, heightSegments );
		var material = new THREE.MeshPhongMaterial();
		var mesh = new THREE.Mesh( geometry, material );
		mesh.name = 'Plane ' + ( ++ meshCount );

		mesh.rotation.x = - Math.PI/2;

		editor.addObject( mesh );
		editor.select( mesh );

	} );

	cube.onClick( function () {

		var width = 100;
		var height = 100;
		var depth = 100;

		var widthSegments = 1;
		var heightSegments = 1;
		var depthSegments = 1;

		var geometry = new THREE.CubeGeometry( width, height, depth, widthSegments, heightSegments, depthSegments );
		var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial() );
		mesh.name = 'Cube ' + ( ++ meshCount );

		editor.addObject( mesh );
		editor.select( mesh );

	} );
	circle.onClick( function () {

		var radius = 75;
		var widthSegments = 32;
		var heightSegments = 16;

		var geometry = new THREE.SphereGeometry( radius, widthSegments, heightSegments );
		var mesh = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial() );
		mesh.name = 'Sphere ' + ( ++ meshCount );

		editor.addObject( mesh );
		editor.select( mesh );
	} );
	another_child.add(circle);
	another_child.add(rectangle);
	another_child.add(square);
	another_child.add(cube);

	container.add(another_child);

	return container;

}

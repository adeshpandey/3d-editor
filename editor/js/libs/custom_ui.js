UI.Circle = function ( radius ) {

	UI.Element.call( this );

	var dom = document.createElement( 'a' );
	dom.className 			= 'label';
	dom.style.border		=	"1px solid #333";
	dom.style.borderRadius	= 	"50%";

	this.dom = dom;
	this.setRadius( radius );

	return this;

};

UI.Circle.prototype = Object.create( UI.Element.prototype );

UI.Circle.prototype.setRadius = function ( value ) {

	if ( value !== undefined ) {

		this.dom.style.width = this.dom.style.height = value;

	}

	return this;

};

UI.Rectangle = function ( w,h ) {

	UI.Element.call( this );

	var dom = document.createElement( 'a' );
	dom.className 			= 'label';
	dom.style.border		=	"1px solid #333";

	this.dom = dom;
	this.setSize( w,h);

	return this;

};

UI.Rectangle.prototype = Object.create( UI.Element.prototype );

UI.Rectangle.prototype.setSize = function ( w,h ) {

	if ( w !== undefined  && h!=undefined) {
		this.dom.style.width			=  	w;
		this.dom.style.height 		= 	h;
	}

	return this;

};

UI.Cube = function ( w,h,img ) {

	UI.Element.call( this );

	var dom = document.createElement( 'img' );
	dom.src = 'css/cube.png';
	dom.className 			= 'cube';

	this.dom = dom;
	this.setSize( w,h);

	return this;

};

UI.Cube.prototype = Object.create( UI.Element.prototype );

UI.Cube.prototype.setSize = function ( w,h ) {

	if ( w !== undefined  && h!=undefined) {
		this.dom.style.width			=  	w;
		this.dom.style.height 		= 	h;
	}

	return this;

};